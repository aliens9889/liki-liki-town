package ISIS.Code.Matriz;

public class ParametrosConstruccion {

	public static final String SALUD = "inf_salud";
	public static final String EDUCACION = "inf_educacion";
	public static final String VIOLENCIA = "inf_violencia";
	public static final String DELINCUENCIA = "inf_delincuencia";
	public static final String EMPLEO = "inf_empleo";
	public static final String DEPORTE = "inf_deporte";
	public static final String SEGURIDAD = "inf_seguridad";
	public static final String ENTRETENIMIENTO = "inf_entretenimiento";
	public static final String CONTAMINACION = "inf_contaminacion";
	public static final String TAMANO = "inf_tamano";
	public static final String TIPO = "tipo";
	public static final String SIGMA = "sigma";
	public static final String COSTO = "costo";
	public static final String HABITANTES = "habitantes";
	public static final String ALQUILER = "alquiler";
	public static final String TIEMPO = "ms";
	public static final String XP = "xp";
	
}
